﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public List<Customer> Parse()
        {
            //Parse data
            return new List<Customer>();
        }
        public IEnumerable<Customer> Parse(string fileName)
        {
            //Parse data
            XElement users = XElement.Load(fileName);
            int maxId = users.Descendants("Customer").Max(x => (int)x.Element("Id"));
            IEnumerable<Customer> listCustomers = from item in users.Descendants("Customer")
                                                  select new Customer()
                                                  {
                                                      Id = (int)item.Element("Id"),
                                                      FullName = (string)item.Element("FullName"),
                                                      Email = (string)item.Element("Email"),
                                                      Phone = (string)item.Element("Phone")
                                                  };
            return listCustomers;
        }
    }
}