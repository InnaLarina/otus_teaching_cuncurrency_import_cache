﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Bogus;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.WebAPI.Models;


namespace ClientWebAPI
{
    class Program
    {
        static HttpClient client = new HttpClient();
        private const string APP_PATH = "https://localhost:44381/";
        static void Main(string[] args)
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static Faker<Customer> CreateFaker()
        {
            var id = 1;
            var customersFaker = new Faker<Customer>("ru")
                .CustomInstantiator(f => new Customer()
                {
                    Id = id++
                })
                .RuleFor(u => u.FullName, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }
        static async Task RunAsync()
        {
            //Генерация случайного клиента
            Customer customer = CreateFaker();
            //Добавление случайного клиента
            client.BaseAddress = new Uri(APP_PATH);
            var result = await client.PostAsJsonAsync("api/values", customer);
            Console.WriteLine("Введите Id Клиента");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                result = await client.GetAsync("api/values/" + id);
                var str = result.Content.ReadAsStringAsync().ConfigureAwait(false);
                string str_result = str.GetAwaiter().GetResult();
                if (str_result == "null")
                    Console.WriteLine("Нет клиента с таким Id");
                else
                    Console.WriteLine("Клиент: " + str_result);
            }
            catch (OverflowException)
            {
                Console.WriteLine("Число вне диапазона целого числа.");
            }
            catch (FormatException)
            {
                Console.WriteLine("Введено не целое число.");
            }

            Console.ReadKey();
        }
    }
}
