﻿using System;
using System.Diagnostics;
using System.IO;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName = @"D:\C#Education\Projects\10.xml";
        private static string _workingDirectory= @"D:\C#Education\Projects";
        private static int _dataCount = 100;
        private static int _wayStart = 1;

        static void Main(string[] args)
        {
            /*if (!TryValidateAndParseArgs(args))
                return;*/
            
            Console.WriteLine("Generating xml data...");
            if (_wayStart == 1)
            {
                var generator = GeneratorFactory.GetGenerator(_dataFileName, _dataCount);

                generator.AsyncGenerate();
            }
            else 
            {
                Console.WriteLine("Process start...");
                Console.WriteLine(_dataFileName);
                ProcessStartInfo infoStartProcess = new ProcessStartInfo();

                infoStartProcess.WorkingDirectory = _workingDirectory;
                infoStartProcess.FileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
                infoStartProcess.Arguments = _dataFileName + " " + _dataCount.ToString();
                Process.Start(infoStartProcess);

            }
            Console.WriteLine(_dataFileName);
            Console.WriteLine($"Generated xml data in {_dataFileName}...");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                if(!args[0].Contains(".xml"))//3
                _dataFileName = Path.Combine(/*_dataFileDirectory*/_workingDirectory, $"{args[0]}.xml");
                else _dataFileName = Path.Combine(_workingDirectory, $"{args[0]}");

            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }
            
            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out _dataCount))
                {
                    Console.WriteLine("Data must be integer");
                    return false;
                }
            }

            if (args.Length > 2)
            {
                if (!int.TryParse(args[2], out _wayStart)||!((_wayStart==1)||(_wayStart == 2)))
                {
                    Console.WriteLine("wayOfStart must be integer and equal 1 or 2");
                    return false;
                }
            }
            return true;
        }
    }
}