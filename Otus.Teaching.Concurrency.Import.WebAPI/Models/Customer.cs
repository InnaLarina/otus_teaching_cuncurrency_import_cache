﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}