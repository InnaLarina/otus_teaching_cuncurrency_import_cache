﻿using System.Data.Entity;


namespace Otus.Teaching.Concurrency.Import.WebAPI.Models
{
    public class CustomerContext: DbContext
    {
        public DbSet<Customer> Customers { get; set; }
    }
}