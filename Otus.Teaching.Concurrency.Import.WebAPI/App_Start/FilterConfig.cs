﻿using System.Web;
using System.Web.Mvc;

namespace Otus.Teaching.Concurrency.Import.WebAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
