﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Http;
using Otus.Teaching.Concurrency.Import.WebAPI.Models;
using Otus.Teaching.Concurrency.Import.WebAPI.Services;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Controllers
{
    public class ValuesController : ApiController
    {
        CustomerContext db = new CustomerContext();
        private AppCache appCache;

        public ValuesController()
        {
            appCache = new AppCache();
        }
        public IEnumerable<Customer> GetCustomers()
        {
            return db.Customers;
        }

        public Customer GetCustomer(int id)
        {
            /*Customer customer = db.Customers.Find(id);
            return customer;*/
            var result = appCache.GetValue(id);
            // если нет в кэше
            if (result == null)
            {
                // берем из БД
                result = db.Customers.Find(id);
                // добавляем в кэш
                appCache.Add(result);
            }
            return result;
        }

        
        public void Post([FromBody]Customer customer)
        {
            db.Customers.Add(customer);
            db.SaveChanges();
        }

    }
}