﻿using Otus.Teaching.Concurrency.Import.WebAPI.Models;
using System;
using System.Runtime.Caching;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Services
{
    public class AppCache
    {
        public Customer GetValue(int id)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Get(id.ToString()) as Customer;
        }

        public bool Add(Customer value)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Add(value.Id.ToString(), value, DateTime.Now.AddMinutes(10));
        }

    }
}