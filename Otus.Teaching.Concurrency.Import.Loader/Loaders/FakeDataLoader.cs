using System.Collections.Generic;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader
        : IDataLoader
    {
        private string _dataFilePath;

        public FakeDataLoader(string dataFilePath)
        {
            _dataFilePath = dataFilePath;
        }

        public void LoadData()
        {
            XmlParser parser = new XmlParser();
            IEnumerable<Customer> listCustomers = parser.Parse(_dataFilePath);
            listCustomers.CallThreads(2);
        }

    }
}